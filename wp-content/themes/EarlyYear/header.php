<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<header id="masthead" class="site-header light-header <?php if ( is_post_type_archive( 'scholaris' ) || is_search() ) {echo "light-header"; }?><?php if( get_field('light_header') == 'yes' || is_single() ){echo "light-header"; } ?>" role="banner">
		<div class="wrap_main">
			<div class="header-inner">
				<div class="header-logo">
					<a href="<?php echo home_url(); ?>">
						<?php /*<img class="fill-logo" src="<?php echo ot_get_option('light_logo'); ?>"> */?>
						<img class="white-logo" src="<?php echo ot_get_option('logo'); ?>">
					</a>
				</div>
				<div class="header-nav-menu header-menu">
					<div class="show-menu">
						<?php if ( has_nav_menu( 'top' ) ) : ?>
							<div class="navigation-top">
								<div class="nav-inner">
									<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
								</div><!-- .wrap -->
							</div><!-- .navigation-top -->
						<?php endif; ?>
					</div>					
					<div id="header_right_search" class="h-search">
						<div class="cover_header_right">
							<a href="#" class="search_show"></a>
							<div class="searchBox last">
								<a href="#">
									<form role="search" method="get" id="searchform"
										class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
										<div>
											<input type="text" value="<?php echo get_search_query(); ?>" placeholder="Search" name="s" class="search_box" id="s" />
											 <input type="submit" id="searchsubmit"/>
										</div>
									</form>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="header-social">
					<a href="<?php echo ot_get_option('facebook'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="<?php echo ot_get_option('instagram'); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<a href="<?php echo ot_get_option('twitter'); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="<?php echo ot_get_option('linkedin'); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</div>
			</div>
		</div> 

	</header><!-- #masthead -->
	<div class="hoverShowBackground"></div>

	<div class="site-content-contain <?php if(is_page(array('528','73','542','547','553'))){ echo 'clud-pos-same'; } ?>">
		<div id="content" class="site-content">
