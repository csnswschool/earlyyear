<?php
/**
 *
*/

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="wrap">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
			    <?php if(function_exists('bcn_display'))
			    {
			        bcn_display();
			    }?>
			</div>
		</div>
		<div class="resource-page-title-section">
			<div class="wrap">
				<h1 class="page_main_heading"><?php the_title();?></h1>
				<div class="resource-category">
					<ul>
						<li><a href="#videoId"><span></span>Videos</a></li>
						<li><a href="#pdfId"><span></span>PDFs</a></li> 
						<li><a href="#podcastsId"><span></span>Podcasts</a></li>  
					</ul>
				</div>
			</div>
		</div>
		<div id="videoId" class="resource-video-section">
			<div class="wrap">
				<div class="resource_top_title">
					<h2 class="fonts48"><span></span> <?php the_field('video_main_title');?></h2>
				</div>
				<div class="resource-video-box-row">
					<ul id="rsrceVdoLst">
					<?php if(have_rows('resource_video_box')):
						$vdocnt = 1;
						while(have_rows('resource_video_box')): the_row();?>
						<li>
							<div class="resource-video-box-column">
								<div class="resource-video-img">
									<?php /*<img src="<?php the_sub_field('video_box_img');?>"> */?>
									<iframe src="https://player.vimeo.com/video/<?php the_sub_field('video_box_link');?>?loop=1" width="640" height="300" frameborder="0" rel="0" allowfullscreen="allowfullscreen"></iframe>
									<?php /*<a href="#vdoCnt<?php echo $vdocnt; ?>" class="fancybox-inline"><i class="fa fa-play-circle" aria-hidden="true"></i></a> */?>
								</div>
								<div class="resource-video-box-text">
									<div class="video-box-title-upper">
										<div class="box-for">
											<?php/*
												$relation = get_sub_field('name_of_relationship');
												if($relation == 'parents'){
													echo "<span></span> Parents";
												}
												if($relation == 'teachers'){
													echo "<span></span> Teachers";
												}*/
											?>
										</div>
										<h3 class="fonts28"><?php the_sub_field('video_box_title');?></h3>
										<div class="box-duration">
											<?php the_sub_field('video_long_min');?><span></span>
										</div>
									</div>
									<p class="video-box-content"><?php the_sub_field('video_box_content');?></p>
									<a href="<?php the_sub_field('video_watch_now_link');?>" class="fancybox-youtube"><span></span> LEARN MORE</a>
									<?php /*
									<div style="display:none" class="fancybox-hidden">
										<div id="vdoCnt<?php echo $vdocnt; ?>" class="hentry" style="width:100%;">	
											<iframe src="https://player.vimeo.com/video/<?php the_sub_field('video_box_link');?>" width="1150" height="650" frameborder="0" rel="0" allowfullscreen="allowfullscreen"></iframe>
											
										</div>
									</div>
									*/?>
								</div>	
							</div> 
						</li>
					<?php $vdocnt++; endwhile; endif;?>
					</ul>
					<div id="loadMoreVdo" class="view_link">Load more</div>
				</div>
			</div>
		</div>
		<div id="pdfId" class="resource-pdf-section">
			<div class="wrap">
				<div class="resource_top_title">
					<h2 class="fonts48"><span></span> <?php the_field('pdf_main_title');?></h2>
				</div>
				<div class="resource-pdf-box-row">
					<ul id="rsrcePdfLst">
					<?php if(have_rows('pdf_box')):
						while(have_rows('pdf_box')): the_row();?>
							<li>
								<a href="<?php the_sub_field('pdf_download_link');?>" target="_blank" class="resource-pdf-box-column">
									<div class="resource-pdf-box-column-in">
										<h3 class="fonts28"><?php the_sub_field('pdf_box_title');?></h3>
										<p><span></span> Download</p>
									</div>
								</a>
							</li>
						<?php endwhile; endif;?>
					</ul>
					<div id="loadMorePdf" class="view_link">Load more</div>	
				</div>
			</div>
		</div>
		<div id="podcastsId" class="resource-podcast-section">
			<div class="wrap">
				<div class="resource_top_title ">
					<h2 class="fonts48"><span></span> <?php the_field('podcasts_main_title');?></h2>					
				</div>
				<div class="resource-podcst-box-row">
					<ul id="rsrcePdcstLst">
					<?php if(have_rows('podcasts_box')):
						while(have_rows('podcasts_box')): the_row();?>
							<li>
								<div class="resource-podcast-box-column">
									<div class="podcasts-box-title-upper">
										<div class="box-for">
											<?php/*
												$relation = get_sub_field('name_of_relationship');
												if($relation == 'parents'){
													echo "<span></span> Parents";
												}
												if($relation == 'teachers'){
													echo "<span></span> Teachers";
												}*/
											?>	
										</div>
										<div class="box-duration">
											<?php the_sub_field('podcasts_long_time');?><span></span>
										</div>
									</div>
									<h3 class="fonts28"><?php the_sub_field('podcasts_box_title');?></h3>
									<p class="podcast-box-content"><?php the_sub_field('podcasts_box_content');?></p>
									<a href="<?php the_sub_field('podcasts_box_listen_link');?>"><span></span> Listen Now</a>
								</div>
							</li>
					<?php endwhile; endif; ?>
					</ul>
					<div id="loadMorePdcs" class="view_link">Load more</div>
				</div>
			</div>
		</div>
		
		<div class="page_shaps">
			<div class="top_balun"></div>
			<div class="fly_yelo_one"></div>
			<div class="top_balun"></div>
			<div class="about_kite_one"></div>
			<div class="about_kite_two"></div>
			<div class="balun-two"></div> 
		</div> 
		
	</main>
</div>
<?php 
get_footer(); 