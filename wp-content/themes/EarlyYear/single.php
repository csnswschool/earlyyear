<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

get_header(); ?>


	<div id="primary" class="content-area blog-sec-cvr">
		<main id="main" class="site-main" role="main">

			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post(); 
				$image = wp_get_attachment_url(get_post_thumbnail_id($parent->post->ID));
				?>

						<div class="blog-cnt-cvr">
							<div class="blg-img" style="background-image: url('<?php echo $image; ?>');"> 
								<?php //the_post_thumbnail(); ?>
								<div class="blg-ttl">
									<div class="blog_title_wrap">
										<h1 class="page_main_heading"><?php the_title(); ?></h1>
										<div class="page_main_title fonts28"><?php the_excerpt(); ?></div>
									</div>	
								</div>	
							</div>
							<div class="blog-cnt-show">
								<div class="wrap">
									<div class="blog_wrap_content">
										<div class="blg-date">
											<span class="blg-bold">Blog </span> | <span><?php echo get_the_date('M d, Y'); ?></span>
										</div>
										<div class="blg-cnt">
											<?php the_content(); ?>
										</div>
									</div> 
								</div>
							</div>
						</div>
				

				<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
 
<div class="page_shaps">
	<div class="top_balun"></div>
	<div class="fly_yelo_one"></div>
	<div class="fly_yelo_two"></div>
	<div class="about_kite_one"></div>
</div> 
  
<?php
get_footer();
