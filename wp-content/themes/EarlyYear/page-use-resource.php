<?php
/**
 * Template Name: Use of Resources Template Page
 * Use of Resources page template file
 *
 */

get_header(); ?>
<div class="wrap">
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">	
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
		    <?php if(function_exists('bcn_display'))
		    {
		        bcn_display();
		    }?>
		</div>
		<div class="use-resouece-title-section">
			<div class="use-resouece-inner">
				<?php
					while ( have_posts() ) :
						the_post();?>
						<div class="use-res-title">
							<h1 class="page_main_heading"><?php the_title();?></h1>
						</div>
						<div class="use-res-content">
							<?php the_content();?>
						</div>	
				<?php endwhile; // End the loop.
					?>	
			</div>
		</div>
		<div class="use-res-download-section">
			<div class="use-res-download-boxarea">
				<?php if(have_rows('use_resource_download_box')):
					while(have_rows('use_resource_download_box')): the_row();?>
							<div class="use-res-dwnld-lbl">
							<h3 class="page_small_title"><?php the_sub_field('use_resource_download_label_title'); ?></h3>
								<a href="<?php the_sub_field('use_resource_download_link');?>" target="_blank" class="use-res-download-box">  
									<div class="use-res-download-immer">
										<h4 class="fonts28"><?php the_sub_field('use_resource_download_box_title');?></h4>
										<p><span></span> Download</p>
									</div>
								</a>
							</div> 
				<?php endwhile; endif; ?>
			</div>
		</div>
		<div class="use-res-video-section">
			<div class="use-res-video-in">
				<?php if(have_rows('use_resource_video')):
					$vdocnt = 1;
					while(have_rows('use_resource_video')): the_row();?>
						<div class="use-res-video-box">
							<div class="use-res-vdo-lbl">
								<h3 class="page_small_title"><?php the_sub_field('video_iframe_label'); ?></h3>
							</div>
							<div class="use-res-vdo-iframe">
								<a href="<?php the_sub_field('video_iframe_link'); ?>" target="_blank">
									<?php the_sub_field('video_iframe'); ?>
								</a>
							</div>
							<!-- <img src="<?php the_sub_field('video_image');?>">
							<a href="#vdoCnt<?php echo $vdocnt; ?>" class="fancybox-inline"><i class="fa fa-play-circle" aria-hidden="true"></i></a> -->
						</div>
						<!-- <div style="display:none" class="fancybox-hidden">
							<div id="vdoCnt<?php echo $vdocnt; ?>" class="hentry" style="width:460px;max-width:100%;">
								<?php the_sub_field('video_data_show');?>
							</div>
						</div> -->
				<?php $vdocnt++; endwhile; endif;?>
			</div>
		</div>
	</div>
</main>
</div>

<div class="page_shaps">
	<div class="top_balun"></div>
	<div class="fly_yelo_one"></div>
	<div class="fly_yelo_two"></div>
	<div class="about_kite_one"></div>
	<div class="about_kite_two"></div>
	<div class="balun-two"></div>
</div>

<?php 
get_footer();