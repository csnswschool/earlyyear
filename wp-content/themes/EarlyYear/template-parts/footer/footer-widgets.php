<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<div class="footer-main">
	<div class="wrap_main">
		<div class="footer-logo footer_div">
			<?php dynamic_sidebar('footer-1'); ?>
		</div>
		
		<div class="footer-two footer_div">
			<?php dynamic_sidebar('footer-2'); ?>
		</div>
		
		<div class="footer-three footer_div">
			<?php dynamic_sidebar('footer-3'); ?>
		</div>
		
		<div class="footer-four footer_div">
			<?php dynamic_sidebar('footer-4'); ?>
		</div>		
		
		<div class="footer-five footer_div">
			<?php dynamic_sidebar('footer-5'); ?>
		</div>

		<div class="footer-six footer_div">
			<div class="footer-social">
					<a href="<?php echo ot_get_option('facebook'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="<?php echo ot_get_option('instagram'); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<a href="<?php echo ot_get_option('twitter'); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="<?php echo ot_get_option('linkedin'); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>	
</div>
<div class="clear"></div>

