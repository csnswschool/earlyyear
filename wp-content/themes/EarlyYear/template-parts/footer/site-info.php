<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="scrollup-arrow scrollup">
	<a href="#" class="scrollupsss"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
</div>
<?php if(is_page(383) || is_page(1271) || is_archive('archive-scholaris') || is_single('single-scholaris')){?>
<style type="text/css">
	.scrollup-arrow.scrollup{display: none!important;}
</style>
<div class="scrollup-arrow scrollup-content">
	<a href="#scrolltop-content" class="scrollupsss"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
</div>
<?php } ?>
<div class="site-info">
	<div class="wrap_main">
		<div class="site-info-right">
			<ul class="site-info-menu">
				<li><a href="<?php echo home_url();?>/privacy-policy/">Privacy</a></li>
				<li><a href="#">Accountability</a></li>
				<li><a href="#">Registration</a></li>
			</ul>
			<div class="copyright">© 2019 | Catholic Schools NSW</div> 
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div><!-- .site-info -->