<?php
/**
 * Template Name: Main Box Toppics pages
 *
 */
get_header();
$getID = get_the_ID();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">	
			<div class="wrap">
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
				    <?php if(function_exists('bcn_display'))
				    {
				        bcn_display();
				    }?>
				</div>
			</div>
			<div class="literacy-title">
				<div class="wrap">
					<?php
					while ( have_posts() ) :
						the_post();?>
						<div class="literacy-main-title">
							<h1 class="page_main_heading"><img src="<?php the_field('main_title_icon_boxes', $getID); ?>"> <?php the_title();?></h1>
						</div>
						<div class="page_main_title fonts28">
							<?php the_content();?>
						</div>

					<?php endwhile; // End the loop.
					?>
				</div>
			</div>
		
			<div class="literacy-boxes-section">
				<div class="wrap">
					<div class="literacy-boxes-in">
					<?php if(have_rows('literacy_box', $getID)):
						while(have_rows('literacy_box', $getID)): the_row();?>
							<div class="literacy-box">
								<a href="<?php the_sub_field('link_this_page');?>">
									<div class="iteracy_title_atra"><h4 class="iteracy_title fonts36"><?php the_sub_field('literacy_box_title');?></h4></div>
									<div class="literacy-box-hover">
										<div class="literacy-box-hover-in">
											<h4 class="iteracy_title fonts28"><?php the_sub_field('literacy_box_title');?></h4>
											<p><?php the_sub_field('literacy_box_hove_content');?></p>
										</div>
									</div>
								</a>
							</div>
					<?php endwhile; endif;?>
					</div>
				</div>
			</div>
			
		</main><!-- #main -->
	</div><!-- #primary -->
	
	<div class="page_shaps">
		<div class="top_balun"></div>
		<div class="fly_yelo_one"></div>
		<div class="fly_yelo_two"></div>
		<div class="about_kite_one"></div>
	</div>
	
<?php
get_footer();