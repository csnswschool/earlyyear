<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
$page_id= get_queried_object_id();
$JohnTxt = ot_get_option('john_text');
?>
<div class="above-footer-sec">
    <div class="show-above-footer b_radius" style="background-image: url('<?php the_field('footer_above_background',$page_id); ?>');">
        <div class="wrap">
              <div class="above-footer-text"><p><?php echo ot_get_option('footer_above_text'); ?><?php if(is_front_page()){ echo '<br><span class="author-name">'.$JohnTxt.'</span>'; }?></p></div>
        </div>
    </div>
</div>



<footer id="colophon" class="site-footer" role="contentinfo">
	
		<?php
		get_template_part( 'template-parts/footer/footer', 'widgets' );
		get_template_part( 'template-parts/footer/site', 'info' );
		?>
	
</footer><!-- #colophon -->
</div><!-- #content -->
</div><!-- .site-content-contain -->
</div><!-- #page -->
<script type="text/javascript">
	jQuery(document).ready(function($){
		/* header search */  
	   $('#header_right_search a.search_show').click( function(e) {
			e.preventDefault();
			e.stopPropagation();
			$('#header_right_search .searchBox').toggle(300);
			 $('#header_right_search .searchBox').toggleClass('active-search-btn');
		});
		$('#header_right_search .searchBox').click( function(e) {
			e.stopPropagation(); 
		});    
		$('#header_right_search .searchBox').hide();




		/**/
		jQuery(window).scroll(function() {    
		    var scroll = $(window).scrollTop();

		    if (scroll >= 15) {
		        jQuery('header#masthead').addClass('sticky-header');
		        jQuery('header#masthead').addClass('header-hover');
		    } else {
		      jQuery('header#masthead').removeClass('sticky-header');
		      jQuery('header#masthead').removeClass('header-hover');
		    }
		});

		/* load more search*/
		 jQuery("ul#ubermenu-nav-main-2-top").hover(function () {
		      jQuery('.site-content-contain').addClass("container-background");
		      jQuery('.hoverShowBackground').show();
		 }, function() {
		      jQuery('.site-content-contain').removeClass("container-background");
		      jQuery('.hoverShowBackground').hide();
		});

		/**/
		 var vdo_li = $("#rsrceVdoLst li").size();
		    vdo_x = 3;
		    $('#rsrceVdoLst li:lt('+vdo_x+')').show();
		    $('#loadMoreVdo').click(function () {
		        vdo_x = (vdo_x+3 <= vdo_li) ? vdo_x+3 : vdo_li;
		        $('#rsrceVdoLst li:lt('+vdo_x+')').show();		        
		        if(vdo_x == vdo_li){
		            $('#loadMoreVdo').hide();
		        }
		    });
		 /**/  

		 /**/
		 var pdf_li = $("#rsrcePdfLst li").size();
		    pdf_x = 3;
		    $('#rsrcePdfLst li:lt('+pdf_x+')').show();
		    $('#loadMorePdf').click(function () {
		        pdf_x = (pdf_x+3 <= pdf_li) ? pdf_x+3 : pdf_li;
		        $('#rsrcePdfLst li:lt('+pdf_x+')').show();		        
		        if(pdf_x == pdf_li){
		            $('#loadMorePdf').hide();
		        }
		    });
		 /**/  

		 /**/
		 var pdcs_li = $("#rsrcePdcstLst li").size();
		    pdcs_x = 3;
		    $('#rsrcePdcstLst li:lt('+pdcs_x+')').show();
		    $('#loadMorePdcs').click(function () {
		        pdcs_x = (pdcs_x+3 <= pdcs_li) ? pdcs_x+3 : pdcs_li;
		        $('#rsrcePdcstLst li:lt('+pdcs_x+')').show();		        
		        if(pdcs_x == pdcs_li){
		            $('#loadMorePdcs').hide();
		        }
		    });
		 /**/   
	});
</script>
<style type="text/css">
	#rsrceVdoLst li{ display:none; } 
	#rsrcePdfLst li{ display:none; } 
	#rsrcePdcstLst li{ display:none; } 
</style> 

<?php wp_footer(); ?>

</body>
</html>

