<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	
		<div class="cloud-main">
			<div class="cloud-cover">
				<?php $cloudCount = 1; if(have_rows('cloud_boxes')):
					while(have_rows('cloud_boxes')): the_row();?>
						<div class="cloud cloud<?php echo $cloudCount; ?>">
							<a href="<?php echo the_sub_field('cloud_text_link'); ?>"><span class="cloud-title fonts36"><?php the_sub_field('cloud_box_text');?></span></a>
						</div>
				<?php $cloudCount++; endwhile; endif;?>
			</div>
			<div class="early-years-title">
				<div class="wrap">
					<h2 class="header_title fonts65"><?php the_field('learning_sec_title');?></h2>
				</div>
			</div>
		</div>  
		
		<div class="learning-sec"> 
			<div class="wrap">
				<div class="ques-mark-box">
					<i class="fa fa-question-circle" aria-hidden="true"></i>
					<h6 class="fonts24"><?php the_field('learning_ques_box_title');?></h6>
				</div>
				<div class="learning-description fonts28">
					<p><?php the_field('learning_sec_description');?></p>
				</div>
			</div>
		</div>
		<div class="round-boxe-area">
			<div class="round-boxes-inner">
				<?php $i=0; $roundBxCnt = 1;
				if(have_rows('round_boxes')):
					while(have_rows('round_boxes')): the_row();?>
						<?php if ($i%2==0) {?>								
							<div class="round-img-box-left round_box  round_box<?php echo $roundBxCnt; ?>">
								<div class="round-img round-img-box">
									<div class="logo-box">
										<div class="logo-box-inner">
											<a href="<?php the_sub_field('round_box_cloud_title_link');?>">
												<img src="<?php the_sub_field('round_box_cloud_icon_class');?>">
												<h6 class="fonts28"><?php the_sub_field('round_box_cloud_title');?></h6>
											</a>
										</div>
									</div>
									<div class="img">
										<div class="img-inner">
											<img id="imgleft" src="<?php the_sub_field('round_box_image');?>" >
										</div>
									</div>
								</div>
								<div id="mySidenavRight" class="round-box-hover-right round-box-hover">
									<div class="round-box-hover-in">
									<h4 class="fonts38"><?php the_sub_field('round_box_hover_title');?></h4>
									<?php if(have_rows('round_box_hover_data')): while(have_rows('round_box_hover_data')): the_row();?>
											<div class="round-box-hover-left-info fonts28">
												<a href="<?php the_sub_field('round_box_hover_text_link');?>"><i class="fa fa-hand-o-right" aria-hidden="true"></i><?php the_sub_field('round_box_hover_data_text');?></a>
											</div>
									<?php endwhile; endif; ?>
									<a href="javascript:void(0)" class="closebtn closeNavRight fonts28"><span><i class="fa fa-times" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>	
						<?php } else {?>
							<div class="round-img-box-right round_box round_box<?php echo $roundBxCnt; ?>">								
								<div class="round-img round-img-box">
									<div class="logo-box">
										<div class="logo-box-inner">
											<a href="<?php the_sub_field('round_box_cloud_title_link');?>">
												<img src="<?php the_sub_field('round_box_cloud_icon_class');?>">
												<h6 class="fonts28"><?php the_sub_field('round_box_cloud_title');?></h6>
											</a> 
										</div>
									</div>
									<div class="img">
										<div class="img-inner">
											<img id="imgright" src="<?php the_sub_field('round_box_image');?>" >
										</div>
									</div>	 
								</div>
								<div id="mySidenavLeft" class="round-box-hover-left round-box-hover">
									<div class="round-box-hover-in">
										<h4 class="fonts38"><?php the_sub_field('round_box_hover_title');?></h4>
										<?php if(have_rows('round_box_hover_data')):
										while(have_rows('round_box_hover_data')): the_row();?>
											<div class="round-box-hover-left-info fonts28">
												<a href="<?php the_sub_field('round_box_hover_text_link');?>"><i class="fa fa-hand-o-right" aria-hidden="true"></i><?php the_sub_field('round_box_hover_data_text');?></a>
											</div>											
										<?php endwhile; endif;?>
										<a href="javascript:void(0)" class="closebtn closeNavLeft fonts28"><span><i class="fa fa-times" aria-hidden="true"></i></span></a>
									</div>
								</div>
								
							</div>										
				<?php } $i++; $roundBxCnt++; endwhile; endif;?>
			</div>
		</div>		
		<div class="more-site-info">
			<div class="wrap">
						<div class="more-site-info-box">
							<a href="<?php the_field('more_info_link');?>" class="more-site-title-box"> 
								<div class="flex_center">
								<i class="<?php the_field('more_info_icon');?>"></i>
								<h4 class="more_title fonts24"><?php the_field('more_info_box_title');?></h4>
								</div>
							</a>							
						</div>
						<div class="more-site-info-box">
							<a href="<?php the_field('list_of_reso_home_link');?>" class="more-site-title-box"> 
								<div class="flex_center">
								<i class="<?php the_field('list_of_resor_home_icon');?>"></i>
								<h4 class="more_title fonts24"><?php the_field('list_of_res_home_title');?></h4>
								</div>
							</a>							
						</div>
				<?php if(have_rows('more_info_repeater')):
					while(have_rows('more_info_repeater')): the_row();?>
						<div class="more-site-info-box podcasts-list-shw">
							<a href="<?php the_sub_field('podcasts_link'); ?>" class="more-site-title-box"> 
								<div class="flex_center">
								<i class="<?php the_sub_field('more_info_icon_class');?>"></i>
								<h4 class="more_title fonts24"><?php the_sub_field('more_info_box_title');?></h4>
								</div>
							</a>
							<div class="more-site-info-box-hover">
								<?php if(have_rows('more_info_box_hover')):
									while(have_rows('more_info_box_hover')): the_row();?>
										<span class="fonts28">
											<a target="_blank" href="<?php the_sub_field('more_info_box_hover_data_link');?>"><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php the_sub_field('more_info_box_hover_text');?></a>
										</span>
								<?php endwhile; endif;?> 
							</div> 
						</div>
				<?php endwhile; endif;?>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->
<script>

jQuery(document).ready(function($){
  $("img#imgright").mouseover(function(ev){
    $(this).parent().parent().parent().next("div#mySidenavLeft").css({"width":"80%", "opacity":"1"});
    $(this).parent().parent().parent().next("div#mySidenavLeft").addClass('hvr-shw');
	return false;
  });
  $("img#imgleft").mouseover(function(ev){
    $(this).parent().parent().parent().next("div#mySidenavRight").css({"width":"80%", "opacity":"1"});
    $(this).parent().parent().parent().next("div#mySidenavRight").addClass('hvr-shw');
    $(this).addClass('hover-imgleft');	   
	return false;
  });
  $("a.closeNavLeft").click(function(ev){
    $(this).parent().parent("div#mySidenavLeft").css({"width":"0", "opacity":"0"});
  	$('div#mySidenavLeft').removeClass('hvr-shw');
	return false;
  });
  $("a.closeNavRight").click(function(ev){	
    $(this).parent().parent("div#mySidenavRight").css({"width":"0", "opacity":"0"});
    $('div#mySidenavRight').removeClass('hvr-shw');
	return false;
  });

  $(".podcasts-list-shw").mouseover(function(ev){
  		$('.podcasts-list-shw .more-site-title-box').addClass('active');
  		$('.podcasts-list-shw .more-site-info-box-hover').addClass('active-box');
  });
  $(".podcasts-list-shw").mouseout(function() {
        $('.podcasts-list-shw .more-site-title-box').removeClass('active');
  		$('.podcasts-list-shw .more-site-info-box-hover').removeClass('active-box');
    });
}); 
</script>
<?php
get_footer();
