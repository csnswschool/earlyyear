<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
			    <?php if(function_exists('bcn_display'))
			    {
			        bcn_display();
			    }?>
			</div>
			<div class="page-content">
				<div class="page-content-wrap">
					<?php
					while ( have_posts() ) :
						the_post();?>

						<h1 class="page_main_heading"><?php the_title();?></h1>
						<?php the_content();?>
						

					<?php endwhile; // End the loop.
					?>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<div class="page_shaps">
	<div class="top_balun"></div>
	<div class="fly_yelo_one"></div>
	<div class="fly_yelo_two"></div>
	<div class="about_kite_one"></div>
	<div class="about_kite_two"></div>
</div>


<?php
get_footer();
