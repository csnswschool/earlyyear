<?php
/**
 * Template Name: Speaking & lestening
 * The speaking and learning page template file
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="wrap">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
			    <?php if(function_exists('bcn_display'))
			    {
			        bcn_display();
			    }?>
			</div>
		</div>
		<div class="speaking-title-section">
			<div class="wrap">
				<h2><?php the_title();?></h2>			
				<div class="speaking-subtitle">
					<?php the_content();?>
				</div>
				<a href="<?php the_field('download_button_link');?>">Download Printable Resources</a>
			</div>
		</div>
		<div class="speaking-video-section">
			<div class="wrap">
				<div class="watch-title-section">
					<div class="small-box-1">
						<i class="fa fa-youtube"></i>
						<h4>Watch</h4>
					</div>
					<div class="small-box-2">
						<i class="fa fa-clock"></i>
						<span>1 min</span>
					</div>
				</div>
				<div class="speking-vdo-img" class="fancybox-iframe">
					<img src="<?php the_field('speaking_video_img');?>">
					<a href="#spekingVdoPop" class="fancybox-inline">Click</a>
					<div style="display:none" class="fancybox-hidden">
						<div id="spekingVdoPop" class="hentry" style="width:460px;max-width:100%;">
							<?php the_field('speaking_video_link');?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="curriculum-section">
			<div class="wrap">
				<div class="curriculum-title">
					<i class="fa fa-book"></i>
					<h2><?php the_field('curriculum_title');?></h2>
				</div>
				<div class="curriculum-data-box">
					<?php if(have_rows('curriculum_data_box')):
						while(have_rows('curriculum_data_box')): the_row();?>
							<div class="curriculum-data-box-details">
								<a href="<?php the_sub_field('curriculum_data_box_title_link');?>"><?php the_sub_field('curriculum_data_box_title');?></a>
								<p><?php the_sub_field('curriculum_data_box_content');?></p>
							</div>					
					<?php endwhile; endif;?>
				</div>
			</div>
		</div>
		<div class="families-section">
			<div class="wrap">
				<div class="families-title">
					<i class="fa fa-book"></i>
					<h2><?php the_field('family_title');?></h2>
				</div>
				<div class="family-content-section">
					<div class="family-content-left">
						<div class="family-content-left-title">
							<i class="fa fa-book"></i>
							<h2><?php the_field('family_left_title');?></h2>
							<div class="small-box-data">
								<i class="fa fa-clock"></i>
								<span>1 min</span>
							</div>
						</div>
						<p><?php the_field('family_left_subtitle');?></p>
						<div class="family-content-left-text">
							<p><?php the_field('family_left_content');?></p>
						</div>
					</div>
					<div class="family-content-right">
						<div class="family-content-right-title">
							<i class="fa fa-book"></i>
							<h2><?php the_field('family_right_title');?></h2>
							<div class="small-box-data">
								<i class="fa fa-clock"></i>
								<span>1 min</span>
							</div>
						</div>
						<p><?php the_field('family_right_subtitle');?></p>
						<?php if(have_rows('family_right_content')):
							while(have_rows('family_right_content')): the_row();?>
								<div class="family-content-left-text">
									<i class="fa fa-watch"></i>
									<p><?php the_sub_field('family_right_text');?></p>
								</div>
						<?php endwhile; endif;?>
					</div>
				</div>
			</div>	
		</div>
		<div class="teachers-section">
			<div class="wrap">
				<div class="teachers-title">
					<i class="fa fa-book"></i>
					<h2><?php the_field('teachers_main_title');?></h2>
				</div>
				<div class="teachers-data-box">
					<?php if(have_rows('teachers_box')):
						while(have_rows('teachers_box')): the_row();?>
							<div class="teachers_box-details">
								<div class="teachers_box-title">
									<i class="fa fa-book"></i>
									<h2><?php the_sub_field('teachers_title');?></h2>
									<div class="small-box-data">
										<i class="fa fa-clock"></i>
										<span>1 min</span>
									</div>
								</div>
								<h6><?php the_sub_field('teachers_subtitle');?></h6>
								<p><?php the_sub_field('teachers_content');?></p>
							</div>					
					<?php endwhile; endif;?>
				</div>
			</div>
		</div>
		<div class="know-more-section">
			<div class="wrap">
				<div class="know-more-title">
					<h2><?php the_field('know_more_title');?></h2>
				</div>
			</div>
			<div class="know-more-content">
				<?php if(have_rows('know_more_content')):
					while(have_rows('know_more_content')): the_row();?>
						<div class="know-more-content-box">
							<i class="fa fa-fingure"></i>
							<p><?php the_sub_field('know_more_content_text');?></p>
						</div>
				<?php endwhile; endif;?>
			</div>
		</div>
	</main>
</div>
<?php 
get_footer();