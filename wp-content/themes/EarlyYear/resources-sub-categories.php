<?php
/*
 * Template Name: Sub Categories
*/
get_header();
$getPageId = get_the_ID();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="wrap">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
			    <?php if(function_exists('bcn_display'))
			    {
			        bcn_display();
			    } ?>
			</div>
		</div>
		<div class="speaking-title-section">
			<div class="wrap">
				<?php
					while ( have_posts() ) :
						the_post();
				$dwnldBtn = get_field('download_button_link', $getPageId);
						?>
						<div class="all-sub-cat-headding">
							<div class="literacy-main-title">
								<h1 class="page_main_heading"><?php the_title();?></h1>
							</div>
							<div class="page_main_title fonts28">
								<?php the_content();?>
							</div>
							<div class="download_categoty"><a href="<?php echo $dwnldBtn; ?>" target="_blank">Download Printable Resource</a></div>
						</div>	

					<?php endwhile; // End the loop.
					?>				
			</div>
		</div>

		<div class="speaking-video-section">
			<div class="wrap">
				<div class="watch-title-section">
					<div class="small-box-1">
						<h4 class="fonts48"> 
							<span></span>
							<?php echo get_field('all_cat_vdo_title', $getPageId); ?>
						</h4>
					</div>
					<div class="small-box-2">
						<h4>
							<span></span>
							<?php echo get_field('all_cat_vdo_min', $getPageId); ?>
						</h4>
					</div>
				</div>
				<div class="speking-vdo-img" class="fancybox-iframe">
					<div class="speking-vdo-in">
						<iframe src="https://player.vimeo.com/video/<?php the_field('speaking_video_link', $getPageId);?>?loop=1" width="1150" height="650" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						<!-- <img src="<?php the_field('speaking_video_img', $getPageId); ?>">
						<a href="#spekingVdoPop<?php echo $getPageId; ?>" class="fancybox-inline"><i class="fa fa-play-circle" aria-hidden="true"></i></a> -->
					</div> 
					<!-- <div style="display:none" class="fancybox-hidden">
						<div id="spekingVdoPop<?php echo $getPageId; ?>" class="hentry" style="width:100%; height: auto;">
							<iframe src="https://player.vimeo.com/video/<?php the_field('speaking_video_link', $getPageId);?>" width="1150" height="650" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						</div>
					</div>   -->
				</div>
			</div>
		</div>

		<div class="curriculum-section">
			<div class="wrap">
				<div class="curriculum-section-inner">
					<div class="curriculum-title">
						<h2 class="fonts48"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><b><?php the_field('curriculum_title', $getPageId); ?></b></h2>
					</div>
					<div class="curriculum-data-box">
						<div class="curriculum-data-left col-6 pdr20">
							<div class="curriculum-data-box-details">							
								<?php the_field('curriculum_left_content', $getPageId); ?>
							</div>
						</div>
						<div class="curriculum-data-right col-6 pdl20">
							<div class="curriculum-data-box-details">
								<?php the_field('curriculum_rght_content', $getPageId); ?>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>

		<div class="families-section">
			<div class="wrap">
				<div class="curriculum-section-inner">
					<div class="curriculum-title">
						<h2 class="fonts48"><span></span> <b><?php the_field('family_title', $getPageId);?></b></h2>
					</div>
					<div class="curriculum-data-box family-box-padding">
						<?php
							$cnt = 1;
							if(have_rows('family_box', $getPageId )):
							while(have_rows('family_box', $getPageId )): the_row();
								$title = get_sub_field('title');
								$sub_title = get_sub_field('sub_title');
								$minute = get_sub_field('minute');
								$content = get_sub_field('content');
							?>
							<div class="family-content-section col-6">
								<div class="family-cnt-cvr">
									<div class="family-cnt-title">
										<div class="family-cnt-left">
											<?php if($cnt == 1){echo '<i class="fa fa-book" aria-hidden="true"></i>'; }else{echo '<i class="fa fa-binoculars" aria-hidden="true"></i>
'; } ?>
											<h2 class="fonts48"><?php echo $title; ?></h2>
										</div>
										<div class="small-box-data">
											<span></span>
											<p><?php echo $minute; ?></p>
										</div>
									</div>
									<div class="fmly-cnt-text">
										<?php echo $content; ?>
									</div>
								</div>
							</div>
						<?php $cnt++; endwhile; endif; ?>
					</div>
				</div>
			</div>	
		</div>

		<div class="teachers-section">
			<div class="wrap">
				<div class="curriculum-section-inner">
					<div class="curriculum-title">
						<h2 class="fonts48"><span></span><b><?php the_field('teachers_main_title', $getPageId);?></b></h2>
					</div>
					<div class="curriculum-data-box family-box-padding">
						<?php if(have_rows('teachers_box', $getPageId)):
							$cnt = 1;
							while(have_rows('teachers_box', $getPageId)): the_row();
								$title = get_sub_field('teachers_title');
								$sub_title = get_sub_field('teachers_subtitle');
								$minute = get_sub_field('minute');
								$content = get_sub_field('teachers_content');
							?>
							<div class="tchr-content-section col-6">
								<div class="family-cnt-cvr">
									<div class="family-cnt-title"> 
										<div class="family-cnt-left">
											<?php if($cnt == 1){echo '<img src="http://designbymouse.com.au/earlyyears/wp-content/uploads/2020/07/reflect-icon.png"/>'; }else{echo '<img src="http://designbymouse.com.au/earlyyears/wp-content/uploads/2020/07/Engage-icon.png"/>';}?>
											<h2 class="fonts48"><?php echo $title; ?></h2>
										</div>
										<div class="small-box-data">
											<span></span>
											<p><?php echo $minute; ?></p>
										</div>
									</div>
									<p><?php echo $sub_title; ?></p>
									<div class="fmly-cnt-text">
										<?php echo $content; ?>
									</div>
								</div>
							</div>
						<?php $cnt++; endwhile; endif;?>
					</div>
				</div>
			</div>
		</div>
		<div class="know-more-section">
			<div class="wrap">
				<div class="curriculum-section-inner">
					<div class="curriculum-data-box">
						<div class="know-more-title">
							<h2 class="fonts65">Want to know more?<?php //the_field('know_more_title', $getPageId);?></h2>
						</div>
						<div class="know-more-content">
							<?php if(have_rows('know_more_content', $getPageId)):
								while(have_rows('know_more_content', $getPageId)): the_row();?>
									<div class="know-more-content-box">
										<a href="<?php the_sub_field('know_more_content_link');?>" target="_blank" class="fonts24"><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php the_sub_field('know_more_content_text');?></a>
									</div>
							<?php endwhile; endif;?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</main>
</div>

<div class="page_shaps">
	<div class="top_balun"></div>
	<div class="fly_yelo_one"></div>
	<div class="top_balun"></div>
	<div class="about_kite_one"></div>
	<div class="about_kite_two"></div>
	<div class="balun-two"></div> 
	<div class="fly_yelo_two"></div>
	<div class="about_kite_three"></div>
	<div class="bottom_balun"></div>
	<div class="bottom_balun"></div>
</div>

<?php get_footer(); ?>